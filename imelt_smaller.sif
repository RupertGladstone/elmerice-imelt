!--------------------------------------------------------
$yearinsec = 360.0*24*60*60
$rhoi = 900.0/(1.0e6*yearinsec^2)
$rhow = 1027.0/(1.0e6*yearinsec^2)
$gravity = -9.81*yearinsec^2
$name = "imelt1"
$m = 1.0/3.0
$dt = 864000.0 / yearinsec  ! 864000 sec is ten days



!--------------------------------------------------------
Header
  Mesh DB "imelt_small"
End

!--------------------------------------------------------
Constants
  Buoyancy Use Basal Melt = Logical False
  Water Density = Real $rhow
  Gas Constant = Real 8.314
End

! variable timestepping
$ function timeStepCalc(nt){ \
    dt = 0.00005 * 1.2^nt ;\
    if ( dt>0.5 ) {dt = 0.5;}\
    _timeStepCalc = dt ;\
}

!--------------------------------------------------------
Simulation
  Bandwidth optimization = Logical False
  Coordinate System  = Cartesian 3D
  Simulation Type ="Transient"
!  Simulation Type ="Steady"
  Timestepping Method = "BDF"
  BDF Order = 1
  Timestep Intervals = 1
  TimeStep Size = Variable "Timestep"
     Real MATC "timeStepCalc(tx)"
  Output Intervals = 1
!  Timestep Sizes = Real $dt
  Steady State Min Iterations = 1
  Steady State Max Iterations = 1
  Output Intervals = 1
  Initialize Dirichlet Conditions = Logical False
  Output File = "$name".result" !"
  max output level = 9
  Extruded Mesh Levels = Integer 12
End

!--------------------------------------------------------
Body 1
  Name = "ice"
  Initial Condition = 1
  Equation = 1
  Material = 1
  Body Force = 1
End

Body 2
  Name = "lower_surface"
  Initial Condition = 2
  Equation = 2
  Body Force = 2
  Material = 2
End

Body 3
  Name = "upper_surface"
  Initial Condition = 3
  Material = 3
  Equation = 3
  Body Force = 3
End

!--------------------------------------------------------
Initial Condition 1
  depth = Real 0.0
  height = Real 0.0
  Pressure = Real 0.0
  Velocity 1 = Real 0.0
  Velocity 2 = Real 0.0
  Velocity 3 = Real 0.0
End

Initial Condition 2
  meltRate = Real 0.0
  FS lower = real 0.0
End

Initial Condition 3
  FS upper = real 0.0
End

!--------------------------------------------------------
Body Force 1
  Flow BodyForce 1 = Real 0.0
  Flow BodyForce 2 = Real 0.0
  Flow BodyForce 3 = Real $gravity
End

Body Force 2
  FS lower Accumulation Flux 1 = Real 0.0e0
  FS lower Accumulation Flux 2 = Real 0.0e0
  FS lower Accumulation Flux 3 = Real 0.0e0
End

Body Force 3
  FS upper Accumulation Flux 1 = Real 0.0e0
  FS upper Accumulation Flux 2 = Real 0.0e0
  FS upper Accumulation Flux 3 = Real 0.0e0
End

!--------------------------------------------------------
Material 1
  Sea level = Real 0.0
  Density =  Real $rhoi

  Viscosity Model = String "Glen"
  Viscosity = Real 1.0
  Glen Exponent = Real 3.0
  Critical Shear Rate = Real 1.0e-10
  Rate Factor 1 = Real 1.258e13
  Rate Factor 2 = Real 6.046e28
  Activation Energy 1 = Real 60e3
  Activation Energy 2 = Real 139e3
  Glen Enhancement Factor = Real 1.0

  Constant Temperature = Real -5.0

  Cauchy = Logical True
End

Material 2
  Density =  Real $rhoi
  Max FS lower = Real 0.0
End

Material 3
  Density =  Real $rhoi
End

!--------------------------------------------------------

Solver 1
  Exec Solver = "before all"
  Equation = "read upper surface"
  procedure = "GridDataReader" "GridDataReader"
  Filename = File "DEM_ice_surface_100m_utm30s_v1.nc"

  X Dim Name = String "x"
  Y Dim Name = String "y"
  X Var Name = String "x"
  Y Var Name = String "y"
  Epsilon X = Real 1.0e-4
  Epsilon Y = Real 1.0e-4
  Variable 1 = File "DEM_ice_surface_utm30s_100m.img"
  Target Variable 1 = "FS upper"
  Enable Scaling = Logical False
End

Solver 2
  Exec Solver = "before all"
  Equation = "read lower surface"
  procedure = "GridDataReader" "GridDataReader"
  Filename = File "DEM_basal_ice_draft_100m_utm30s_v1.nc"

  X Dim Name = String "x"
  Y Dim Name = String "y"
  X Var Name = String "x"
  Y Var Name = String "y"
  Epsilon X = Real 1.0e-4
  Epsilon Y = Real 1.0e-4
  Variable 1 = File "DEM_basal_ice_draft_100m_utm30s.img"
  Target Variable 1 = "FS lower"
  Enable Scaling = Logical False
End

Solver 3
  Exec Solver = "never"
!  Exec Solver = "Before All"
  Equation = "MapCoordinateInit"
  Procedure = "StructuredMeshMapper" "StructuredMeshMapper"
  Active Coordinate = Integer 3
  Dot Product Tolerance = Real 0.001
  Minimum Mesh Height = Real 20.0
  Top Surface Variable Name = String FS upper
  Bottom Surface Variable Name = String FS lower
!  Top Surface Variable Name = String DEM_ice_surface_utm30s_100m.img
!  Bottom Surface Variable Name = String DEM_basal_ice_draft_100m_utm30s.img
End

Solver 4
  Exec Solver = String "after saving"
  Equation = String "ResultOutput"
  Procedure = File "ResultOutputSolve" "ResultOutputSolver"
  Save Geometry Ids = Logical True
  Output File Name = File $name
  Output Format = String "vtu"
  Output Directory = File "VTUoutputs"
  Vtu Format = Logical True
End
!  Scalar FIeld 1 = String Height
!  Scalar FIeld 2 = String Depth 
!  Vector Field 1 = String Velocity

!--------------------------------------------------------
Equation 1
  Active Solvers(4) = 1 2 3 4
  Flow Solution Name = String "Flow Solution"
  Convection = Computed
End



!--------------------------------------------------------

Boundary Condition 1
  Name = "sidewall"
  Target Boundaries(4)  = 1 2 3 4

  Normal-Tangential Velocity = Logical True

!  Mass consistent normals = Logical True

  Slip Coefficient 1 = Real 100000.0
  Slip Coefficient 2 = Real 0.00001
  Slip Coefficient 3 = Real 0.00001

!  External Pressure = Variable depth
!    Real MATC "1.0 * rhoi * gravity * tx"

!  Compute Sea Pressure = Logical True

End

Boundary Condition 2
  Name = "lower_surface"

  Target Boundaries(1) = 5
  Body Id = 2

  height = Real 0.0

  Normal-Tangential Velocity = Logical True

  Flow Force BC = Logical True
  
  Slip Coefficient 1 = Variable Coordinate 3
    Real Procedure "ElmerIceUSF" "SeaSpring"

  External Pressure = Variable Coordinate 3
    Real Procedure "ElmerIceUSF" "SeaPressure"

  Compute Sea Pressure = Logical True
  Compute Sea Spring = Logical True

End

Boundary Condition 3
  Name = "upper_surface"

  Target Boundaries(1) = 6
  Body Id = 3

  depth = Real 0.0

End
