
\documentclass[12pt]{article}

\usepackage{graphicx}
\usepackage[margin=1.0in]{geometry}
\usepackage{listings}
\usepackage{hyperref}

\begin{document}

\author{Rupert Gladstone}

\title{Elmer/Ice imelt/Fimbulisen report}


\maketitle


\section{Introduction}

Contains summary information and plots for Elmer/Ice contribution

This document, the additional code file for the project, and the Elmer input and mesh generation
files are contained in a gitlab repository.

The forcing data and plots may be saved elsewhere because git is intended to support code rather
than sharing  large files.

There is also a README file containing technical details for running the Elmer simulations. 



\section{Inputs and spin up}

Modelling inputs (full description can be added as needed, just a brief summary is given for now):

\begin{itemize}

\item{Gridded data in netcdf files from Katrin containing upper and lower surfaces
and ice thickness, UTM coords.}


\item{Density and temperature profiles from Tore.  These are 1D data.}

\item{2D Elmer mesh is generated with approximately uniform resolution in the horizontal for the
region containing valid data from Katrin's netcdf files.}

\item{Elmer mesh is extruded to 20 equally spaced levels.}

\item{Elmer solver input files are in the current directory and define the exact solvers that are used,
physical constants, timestepping information etc.}

\end{itemize}

\begin{figure*}[t]
  \begin{center}
    \includegraphics[width=12.5cm]{setup.png}
  \end{center}
  \caption{    
    Elmer setup. Top panel: temperature distribution.
    Bottom panel: density and mesh resolution.
    Vertical exageration is 10 times.
    The 3D domain is clipped across the channel near to location of M2.
  }
  \label{fig:setup}
\end{figure*}

See Figure~\ref{fig:setup} for visualisation of initial state.
The ice is initially significantly out of floatation (Section~\ref{sec:float}).
Before the start of the simulation the ice body is lowered by 12m
(defined by ``drop'' in Elmer input file).
Then a surface relaxation simulation is carried out.
This is run for one year to allow the domain to approach floatation
(at the large scale that is: this relaxation is too short for large adjustments to the
small scale features). 
Results are analysed at the end of this one year adjustment.




\section{First results}

\begin{figure*}[t]
  \begin{center}
    \includegraphics[width=15cm]{UpperSurfaceChange.png}
  \end{center}
  \caption{
    Upper surface change after the one year adjustment period.
    Left panel: rate of change of ice thickness.
    Right panel: ice thickness (colorscale) and horizontal velocity field at the top layer (arrows).
  }
  \label{fig:usc}
\end{figure*}

Figure~\ref{fig:usc} shows upper surface change rates after the one year adjustment period.
Broadly, this is as expected.
Ice flows from thicker to thinner regions.
The channels are thickening and the thicker regions are thinning.

\begin{figure*}[t]
  \begin{center}
    \includegraphics[width=15cm]{clipWithYZvel.png}
  \end{center}
  \caption{
    Flow field on a plane of constant x near to M2.
    Colours show the z (vertical) component of velocity. 
    Arrows show the velocity vectors projected to the plane of constant x.
  }
  \label{fig:clip}
\end{figure*}

Figure~\ref{fig:clip} shows the vertical structure through a plane of constant x near
to the M2 location. 
Vertical variation in the flow field is small. 
There is a predominantly negative flow in the x-direction (i.e. into the plane from
the viewer's perspective).
The 3D flow is complex and may confuse interpretation of the 2D flow in the plane. 

\begin{figure*}[t]
  \begin{center}
    \includegraphics[width=12cm]{zvelProfiles.png}
  \end{center}
  \caption{
    Vertical velocity relative to ice upper surface at M2 (red line) and
    nearby locations.
    The other locations differ by a couple hundred metres (ish), so the Southern
    most location is near the centre of the channel.
  }
  \label{fig:wprof}
\end{figure*}

The vertical velocity component relative to the upper surface is shown for the M2 location
in Figure~\ref{fig:wprof}.
I think this is directly comparable to the evolution of internal layers.
If I recall correctly the profile at M2 has the opposite sign to the observations and its
variations are about an order of magnitude smaller (less than one mm per year).
So we're not doing a good job of recreating observations here!
The simulated flow field is responding to the geometry and density, basically it is
responding to the bridging stresses.
I don't know how confident we are in the geometry.
Certainly the fact that the intial geometry is far from being at floatation is concerning. 


\section{Height above floatation}
\label{sec:float}

The intial state is significantly above floatation over the whole domain, by around 10-15m. 
Katrin showed a map of this at Tore's suggestion, and her map was consistent with this as
I recall (i.e. we both think the initial geometry is far above floatation). 
This is similar magnitude irrespective of what approximations we make about ice and firn density.
I am not the right person to try to figure out what the cause of this is.

\begin{figure*}[t]
  \begin{center}
    \includegraphics[width=15cm]{haf.png}
  \end{center}
  \caption{
    Height above floatation after the one year adjustment.
    The range is from 2m below floatation (blue) to 2m above floatation (red).
    Ice draft contours (25m spacing) give context.
    The green sphere indicates M2 location. 
  }
  \label{fig:haf}
\end{figure*}

The height above floatation after our one year adjustment simulation is shown in
Figure~\ref{fig:haf}. 
The pattern is not very simple to interpret.
The side of the channel where M2 is located has positive height above floatation.
This is consistent with Tore's sugestion that there may be increased basal melt where
the slope of the lower ice surface is steeper.




\section{Comparison to Vaughan}

\begin{figure*}[t]
  \begin{center}
    \includegraphics[width=16cm]{sigma_yy.png}
  \end{center}
  \caption{
    Deviatoric stress tensor component $\sigma_{yy}$, indicating regions of compression (blue) 
    and extension (red) in the y direction (this is the horizontal direction aligned with the
    plane).
    Units are MPa.
    For comparison to~\cite{Vaughan12}, the range shown here is -200 to 200 kPa. 
    The red dot indicates M2 location. 
  }
  \label{fig:stress}
\end{figure*}

Vaughan's study \cite{Vaughan12} provided a plot of the $xx$ component of the deviatoric
stress tensor, $\sigma_{xx}$, for an idealised 2D domain in which a shelf responds to channels
(his Figure 7). 
Figure~\ref{fig:stress} shows $\sigma_{yy}$ for the current modelling study, which is directly
comparable to Vaughan's $\sigma_{xx}$.
We see a simliar pattern at the upper surface: compression above channels and extension above
thicker regions, but we don't see such a clear pattern of reversal at depth as~\cite{Vaughan12}.
I am not sure why this is, but our more complex geometry, and the fact that it is 3D, make it harder to
interpret this.



\section{Basal melting parameterisation}

\begin{figure*}[t]
  \begin{center}
    \includegraphics[width=12cm]{zvelProfiles2.png}
  \end{center}
  \caption{
    Vertical velocity relative to ice upper surface at M2. 
    The profiles show the evolution over one year with basal melting turned on.
  }
  \label{fig:wprof_bmb}
\end{figure*}

For a further simulation I imposed a crude ice thickness parameteristion for basal melting, $m_b$:
\begin{equation}
m_b = \frac{420-H}{100},
\end{equation}
where $H$ is ice thickness.
This gives melt values close to 1 m/yr near channel apexes and close to zero under thicker ice. 
  
Figure~\ref{fig:wprof_bmb} shows the evolution of the vertical profile of vertical velocity
relative to the upper ice surface 
over a one year transient simulation (with timesteps of one hundredth of a year) with this
basal melting turned on.
The impact is very small.
Having said that, the profiles seem to evolve more rapidly later in the run, so maybe
I should run this for longer.


\section{Outlook}

I think the interaction between basal melting, deviation from floatation,
and the stress/strain field in the ice is very interesting.

One possible avenue would be to establish why our $\sigma_{yy}$ field looks so different
to that from \cite{Vaughan12}.
We could do that by:

\begin{itemize}
\item{Try to repeat Vaughan's idealised 2D experiment (his Figure 7). }
\item{Assuming that works, try the same with our M2 geometry using a 2D profile. }
\item{Then expand to 3D, varying width and length of the domain to learn how more
  distant geometry, and variations in the 3rd dimension, impact on the stress field.}
\end{itemize}

A related avenue would be a more general study of the response of the stress/strain field
to bridging stresses, such as can be caused by spatial variations in SMB and BMB.
I can't think of any reason why regions of the ice shelf would be locally out of floatation
other than local variations in SMB and BMB.
The exception would be near to grounding lines and or pinning points, but I don't think M2 is
near to either of the above. 
So the stress/strain field must be responding to SMB/BMB.
We could impose enhanced melting at the side walls (as a function of slope), instead of at the
apex (as a function of ice draft).
And I could run the current basal melt test for a longer time. 
If we can learn more about this behaviour in an idealised setup we might be able to more
easily explaim what we find for the model domain around M2.

%One avenue would be to explore further the relationship between basal melt
%parameterisations and evolving stress/strain patterns.
%I think I know how to do this with Elmer, though I haven't set it up yet.

One avenue (and perhaps this is something that the Tromso group can do anyway, without my input!) 
would be to figure out why the geometry is intially so far from floatation.
It is hard to believe that this is a real feature, and if an artefact it lowers confidence in
the observed geometry generally. 

Note that the evolution of internal layers in the ice is approximately an order
of magnitude more sensitive to the ice upper surface mass balance (SMB) than it is to
lower surface mass balance (basal melt rates AKA BMB).
This is due to restoration toward floatation. 
I don't know how important it is for us to consider this. 
%I don't think this is an important issue for us, as it would not cause vertical
%variation in the vertical motion of internal layers. 
Reinhard Drews has a paper accepted that mentions this issue. 
Let me know if you would like to see a draft before it is published. 
He circulated the proofs a couple weeks back, so it should be published soon. 


%\begin{figure*}[t]
%  \vspace*{2mm}
%  \begin{center}
%    \includegraphics[width=16cm]{Clip.png}
%    \includegraphics[width=16cm]{clipWithYZvel.png}
%  \end{center}
%  \caption{
%  }
%  \label{fig:clip2}
%\end{figure*}

%\begin{figure*}[t]
%  \vspace*{2mm}
%  \begin{center}
%    \includegraphics[width=16cm]{slices.png}
%  \end{center}
%  \caption{
%  }
%  \label{fig:2}
%\end{figure*}



\bibliographystyle{plain}
\bibliography{/home/gladstone/Papers/IceBib/IceBib}


\end{document}
