
load M2profile0004.dat;
nz = 20;

nt = max(M2profile0004(:,1));

figure(1); clf
hold on

for tt = 1:nt
    first = (tt-1)*nz+1; last  = tt*nz;
    zprof = M2profile0004(first:last,8)*100.0;
    zcoord = M2profile0004(first:last,7);
    zvel_top = zprof(end);
    zprof_rel = zprof - zvel_top;

    plot(zprof_rel,zcoord)
end

xlabel("Vertical velocity relative to upper surface, cm/yr")
ylabel("Height relative to sea level, m")
%xlim([-0.05 0.02])

saveas(gcf,'zvelProfiles2.png')

