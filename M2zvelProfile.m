load M2profile0004.dat;
zprof = M2profile0004(:,8)*100.0;
zcoord = M2profile0004(:,7);
zvel_top = zprof(end);
zprof_rel = zprof - zvel_top;

load profile20004.dat;
zprof2 = profile20004(:,8)*100.0;
zcoord2 = profile20004(:,7);
zvel_top2 = zprof2(end);
zprof_rel2 = zprof2 - zvel_top2;

load profile30004.dat;
zprof3 = profile30004(:,8)*100.0;
zcoord3 = profile30004(:,7);
zvel_top3 = zprof3(end);
zprof_rel3 = zprof3 - zvel_top3;

load profile40004.dat;
zprof4 = profile40004(:,8)*100.0;
zcoord4 = profile40004(:,7);
zvel_top4 = zprof4(end);
zprof_rel4 = zprof4 - zvel_top4;

figure(1); clf
plot(zprof_rel4,zcoord4)
%xlim([-0.05 0.02])
xlabel("Vertical velocity relative to upper surface, cm/yr")
ylabel("Height relative to sea level, m")

hold on
plot(zprof_rel,zcoord)
plot(zprof_rel3,zcoord3)
plot(zprof_rel2,zcoord2)
%legend("M2","A bit further South","South of M2","North of M2")

legend("North of M2","M2","South of M2","A bit further South")

saveas(gcf,'zvelProfiles.png')

